import { CssBaseline } from '@mui/material';
import { createTheme, Stack, ThemeProvider } from '@mui/material';
import { useState, useEffect, useMemo } from 'react';
import { randomQuote } from './utils/api';

import Random, { TopBar } from './components/random';
import QuoteSkeleton from './components/quote_skeleton';

function App() {
  const [random, setRandom] = useState({author: 'Loading...', quote: 'Loading...'})
  const [isLoading, setLoading] = useState(true)

  const [preferredTheme, setTheme] = useState(localStorage.getItem('theme') || 'light');

  const theme = useMemo(
    () => {
    localStorage.setItem('theme', preferredTheme);
    document.documentElement.setAttribute('data-theme', preferredTheme);
    return createTheme({
        palette: {
          mode: preferredTheme,
        },
      })
    },
    [preferredTheme],
  );

  function changeQuote() {
    setLoading(true)
    randomQuote()
    .then(quote => {
      setRandom(s => ({
        ...s,
        id: quote[0]._id,
        author: quote[0].quoteAuthor,
        quote: quote[0].quoteText,
        genre: quote[0].quoteGenre
      }))
      setLoading(false)
    })
  }

  useEffect(changeQuote, [])

  return <ThemeProvider theme={theme}>
    <CssBaseline />
    <Stack sx={{
        justifyContent: 'center',
        minHeight: '100vh',
        alignItems: 'center',
        maxWidth: '600px',
        mx: 'auto',
      }}>
      {
        isLoading ? <QuoteSkeleton />
        :
        <>
          <TopBar
            changeQuote={changeQuote}
            theme={preferredTheme}
            toggleTheme={() => setTheme(s => s === 'light'?'dark':'light')}
          />
          <Random quote={random}/>
        </>
      }
    </Stack>
  </ThemeProvider>
}

export default App;
