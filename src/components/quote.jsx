import { Paper, Typography } from "@mui/material";

export default function Quote({text, tag}) {
  return (
    <Paper elevation={7} sx={{
      borderLeft: 'var(--quote-border)',
      width: '600px',
      padding: '1em 0 1em 2em'
    }}>
      <Typography variant='body1' sx={{width: 'fit-content', padding: '.5em'}}>{text}</Typography>
    </Paper>
  );
}
