import { Box, Button, Paper, Typography } from "@mui/material";
import { Cached, DarkMode, LightMode } from '@mui/icons-material'
import Quote from './quote';

export default function Random({quote}) {
  return <>
    <Quote text={quote.quote}/>
    <Paper elevation={3} sx={{
      width: '100%', maxWidth: '400px',
      marginTop: '1em', padding: '.5em',
      position: 'relative',
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: 'var(--quote-bg)',
        color: 'var(--author-dark-color)',
      }
    }}
    >
      <Typography variant='body1' gutterBottom children={quote.author} sx={{color: 'var(--author-color)', fontWeight: 'bold'}}/>
      {quote.genre && <Typography variant='caption' children={quote.genre} sx={{color: 'var(--secondary-color)'}}/>}
    </Paper>
  </>
}

export function TopBar({changeQuote, theme, toggleTheme}) {
  return <Box mb='1em' gap='1em' display='flex'>
    <Button
      onClick={changeQuote}
      sx={{color: 'var(--primary-color)', backgroundColor: 'var(--primary-bg)'}}
    ><Cached/>Random</Button>
    <Button
      onClick={toggleTheme}
      sx={{color: 'var(--primary-color)', backgroundColor: 'var(--primary-bg)'}}
    >{theme === 'dark' ? <LightMode/> : <DarkMode/>}</Button>
  </Box>
}
