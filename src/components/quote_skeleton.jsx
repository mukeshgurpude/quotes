import { Button, Skeleton } from "@mui/material";

export default function QuoteSkeleton() {
  return <>
    <div style={{display: 'flex', gap: '1em', marginBottom: '1em'}}>
      <Skeleton animation='wave' variant='rectangular'>
        <Button children='random'/>
      </Skeleton>
      <Skeleton animation='wave' variant='rectangular' sx={{}}>
        <Button children='toggle'/>
      </Skeleton>
    </div>
    <Skeleton animation='wave' variant='rectangular' width='600px' height='200px' sx={{maxWidth: '90%', borderLeft: 'var(--quote-border)'}}/>
    <Skeleton animation='wave' variant='rectangular' width='400px' height='100px' sx={{marginTop: '1em', maxWidth: '80%'}} />
  </>
}
