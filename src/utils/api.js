const API_URL = 'https://quote-garden.herokuapp.com/';
const API_VERSION = 'api/v3/';

export function randomQuote() {
  return new Promise((success, reject) => {
    fetch(`${API_URL}${API_VERSION}quotes/random`)
      .then(res => res.json())
      .then(({ data }) => {
        success(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

// Removed from the api
export function authorQuotes(authorName, page=1, limit=10) {
  return new Promise((success, reject) => {
    if (authorName === undefined) {
      reject({ error: "Author is required" });
    }
    fetch(`${API_URL}${API_VERSION}authors/${authorName}?page=${page}&limit=${limit}`)
      .then(res => res.json())
      .then(({ data }) => {
        success(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
