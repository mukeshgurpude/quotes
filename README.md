<h1 align="center">Random Quotes</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://mukeshgurpude.github.io/quotes/">
      Demo
    </a>
    <span> | </span>
    <a href="https://github.com/mukeshgurpude/quotes">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/8Y3J4ucAMQpSnYTwwWW8">
      Challenge
    </a>
  </h3>
</div>
